---
title : Réintroduction de la consigne au Mans - Page principale
tags: Consigne, Laveuse, Le Mans, Sarthe
langs: fr-fr
---
![](https://pad.lescommuns.org/uploads/upload_6e96ed9e501395352e0944fffc4616db.png)

![dessin satyrique emballage](https://img.over-blog-kiwi.com/1/72/81/31/20171001/ob_a348df_zorro.jpg)
*Dessin de Zorro - Charlie Hebdo N°1303 - 12/07/2017*

# Primeverre : un collectif citoyen pour le retour de la consigne en Sarthe !

Jeter les bouteilles et pots en verre n'est pas une fatalité. Pourquoi ne pas les laver et les réutiliser localement plutôt que dépenser une énergie phénoménale à les détruire,  les transporter à l'autre bout de la France, les fondre à haute température pour enfin recycler et redistribuer le verre ?

C'est ce que PrimeVerre veut expérimenter en Sarthe, dans une démarche écologique, économique, citoyenne et de proximité. Nous suivrons la trace des bouteilles de bière, de cidre, de jus, des pots de yahourt, de confiture, de conserves ... produits et consommés en Sarthe pour pouvoir leur inventer, ensemble, de nouveaux chemins. 

Ces chemins partent des producteur·ices qui ont souvent l'envie mais pas les moyens de réutiliser leurs bouteilles et leurs pots. Ils sillonnent les magasins, les bars et les restaurants qui sont des maillons essentiels dans la chaîne de collecte à organiser. Ils passent par nos maisons, nous qui consommons et conservons le souvenir des bouteilles consignées de notre enfance, de nos grand-mères ou de nos voyages. Ils touchent les institutions et les acteurs économiques qui s'intéressent à la réduction des déchets, de l'énergie consommée et de la consommation, aux transitions écologiques et sociales, à l'économie circulaire... A chacune de ces étapes, une transformation est nécessaire. 

:::info 
Réutiliser plutôt que recycler c'est :
- Réduire par 5 la consommation d'énergie
- Réduire par 2 la consommation d'eau
- Créer de l'activité et du lien sur notre territoire

*Source: ADEME, 2018
Fiche du dispositif Meteor, page 4/9*
:::

## Nos actions 

**Cultiver une expertise technique** Prendre connaissance des autres expériences, connaitre les normes et itinéraires techniques. S'intéresser aux étiquettes (colle, papier, chevalet), aux verres, aux modèles de laveuses, à la logistique, au transport pour enfin réaliser le Guide des Bonnes Pratiques d'Hygiène (GBPH : méthode HACCP) de Primeverres.

**Évaluer les besoins en Sarthe** Mener une étude en partant des besoins des producteurs (sans oublier les distributeurs, transporteurs, laveurs et autres partenairesà pour évaluer l’intérêt, le volume potentiel de bouteilles et autres contenants en verre, et les différentes propositions logistiques qui peuvent en découler.

**Construire notre modèle** Veille et étude des modèles économiques et organisationnels existants, établissement de seuils de rentabilité, moyens de financements, besoins en investissement...

**Communiquer** Construire nos outils de communication internes et externe, faire connaître notre initiative, sensibiliser à l’intérêt du réemploi du verre et à ses conséquences (écologiques, économiques et sociales)

:::info
Ca existe ailleurs!
* L'association Bout à Bout a profité de l'existence d'une laveuse industrielle près de Nantes pour restaurer la consigne dans le 44 et le 85
* À Toulouse, l'association "En boîte le plat" propose à des restaurants des contenants en verres consignés pour les plats à emporter, et en assure le lavage.
* En Suisse, le réseau consigne aide producteurs et distributeurs à mettre en place la consigne et fournit un service de lavage.
:::

## Contact
Email : primeverres@le-mans.co
Site web : primeverres.le-mans.co
[Comptes rendus des réunions précédantes et résumé des actions déjà menées](https://pad.lescommuns.org/s/QkZ8ZY7nN).

